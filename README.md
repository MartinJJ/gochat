Golang chat server
==================

A basic golang web socket chat server. Connected users and user messages are relayed back to all clients.

### Dependencies

github.com/gorilla/websocket
Use "go get github.com/gorilla/websocket" to retrieve this package

### Usage

To run the server, in a terminal from this directory

go run go/main.go

1. open frontend/chat.html in a browser
2. (Optional) Set the user name
3. Click the connect button to begin the chat session
4. Messages can be sent using the input box below the chat window and will appear in the chat window

For a server build:

go build go/main.go

The binary will be built into the same directory

### Testing

In a terminal from this directory
1. cd go
2. go test

### Development notes

Each client has its own read/write goroutine to ensure they stay responsive if a single client suffers from poor connection performance.

The websocket is opened on http://localhost:8100/ws

Tested on Windows10 using Chrome
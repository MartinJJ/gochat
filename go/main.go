package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"sync"
	"time"

	"github.com/gorilla/websocket"
)

type NullS struct{}

type Server struct {
	Clients map[*Client]NullS

	OnReceived      func(receivedMsg string)
	ClientConnected func(clientName string)

	// Required to guard clients when in use as concurrent functions may access it
	Mut        sync.Mutex
	HttpServer *http.Server
}

type Client struct {
	Conn *websocket.Conn
	Name string

	ChatEntries   chan ChatEntryMsg
	UserNames     chan UserNamesMsg
	WritePumpDone chan bool
}

type ChatEntryMsg struct {
	Type     string
	Contents string
}

type UserNamesMsg struct {
	Type      string
	UserNames []string
}

// Receives chat messages from a single client
func (s *Server) ReadPump(c *Client) {

	for {
		_, message, err := c.Conn.ReadMessage()

		if err != nil {
			break
		}

		chatLine := fmt.Sprintf("%s: %s", c.Name, message)
		chatMsg := ChatEntryMsg{Contents: chatLine, Type: "ChatEntryMsg"}

		s.Mut.Lock()
		clients := s.Clients
		for c := range clients {
			c.ChatEntries <- chatMsg
		}
		s.Mut.Unlock()

		if s.OnReceived != nil {
			s.OnReceived(chatLine)
		}
	}

	// Signal the write pump to finish
	c.WritePumpDone <- true

	// Lock whilst we remove our client from the list
	s.Mut.Lock()
	delete(s.Clients, c)
	s.BroadcastUserNames()
	s.Mut.Unlock()

	c.Conn.Close()

	fmt.Printf("Server: Closed client %s\n", c.Name)
}

// Sends chat entries and user name lists to the client
func WritePump(c *Client) {

	for {
		var err error

		select {
		case entry := <-c.ChatEntries:
			err = c.Conn.WriteJSON(entry)
		case userNames := <-c.UserNames:
			err = c.Conn.WriteJSON(userNames)
		case <-c.WritePumpDone:
			return
		}

		if err != nil {
			return
		}
	}
}

func (s *Server) GetUserNames() []string {

	clientNames := make([]string, len(s.Clients))
	clientI := 0
	for c := range s.Clients {
		clientNames[clientI] = c.Name
		clientI++
	}
	return clientNames
}

func (s *Server) BroadcastUserNames() {
	userNamesMsg := UserNamesMsg{Type: "UserNamesMsg", UserNames: s.GetUserNames()}

	for c := range s.Clients {
		c.UserNames <- userNamesMsg
	}
}

func (s *Server) NewConnection(w http.ResponseWriter, r *http.Request) {

	upg := websocket.Upgrader{ReadBufferSize: 1024, WriteBufferSize: 1024}
	upg.CheckOrigin = func(r *http.Request) bool { return true }

	ws, err := upg.Upgrade(w, r, nil)
	if err != nil {
		fmt.Println(err)
		return
	}

	// Receive the user name
	_, userName, err := ws.ReadMessage()
	if err != nil {
		fmt.Println(err)
		return
	}

	// Create our new client
	var newClient = Client{
		Conn:          ws,
		Name:          string(userName),
		ChatEntries:   make(chan ChatEntryMsg),
		UserNames:     make(chan UserNamesMsg),
		WritePumpDone: make(chan bool),
	}

	// Lock clients, could be in use in multiple places
	s.Mut.Lock()
	s.Clients[&newClient] = NullS{}

	if s.ClientConnected != nil {
		s.ClientConnected(newClient.Name)
	}

	// Start the client read/write pumps
	go s.ReadPump(&newClient)
	go WritePump(&newClient)

	s.BroadcastUserNames()

	s.Mut.Unlock()
}

func (s *Server) Run(addr string) {
	s.HttpServer = &http.Server{Addr: addr}
	http.HandleFunc("/ws", s.NewConnection)
	err := s.HttpServer.ListenAndServe()

	// Release our handler
	http.DefaultServeMux = new(http.ServeMux)

	if err != nil && err != http.ErrServerClosed {
		log.Fatal(err)
	}
}

func (s *Server) Close() {
	c, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	s.HttpServer.Shutdown(c)
}

func ServerMake() *Server {
	server := Server{Clients: make(map[*Client]NullS)}
	return &server
}

func main() {
	server := ServerMake()
	server.Run(":8100")
}

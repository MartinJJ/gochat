package main

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/gorilla/websocket"
	"log"
	"math/rand"
	"net/url"
	"reflect"
	"strconv"
	"sync"
	"testing"
	"time"
)

func TestServer_RunAndClose(t *testing.T) {
	s := ServerMake()
	go s.Run(":8100")
	defer s.Close()

	time.Sleep(time.Millisecond * 500)

	c, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	if err := s.HttpServer.Shutdown(c); err != nil {
		t.Fatal(err)
	}
}

func ConnectToTestServer(userName string, svrName string) *websocket.Conn {
	u := url.URL{Scheme: "ws", Host: svrName, Path: "/ws"}
	log.Printf("connecting to %s", u.String())

	c, _, err := websocket.DefaultDialer.Dial(u.String(), nil)
	if err != nil {
		log.Fatal("dial:", err)
	}

	c.WriteMessage(websocket.TextMessage, []byte(userName))

	return c
}

func TestServer_NewConnection(t *testing.T) {
	s := ServerMake()
	go s.Run(":8100")
	defer s.Close()

	var connected sync.WaitGroup
	connected.Add(1)

	s.ClientConnected = func(clientName string) {
		connected.Done()
	}

	c := ConnectToTestServer("UN", "localhost:8100")
	defer c.Close()

	connected.Wait()

	clientCount := len(s.Clients)

	if len(s.Clients) != 1 {
		t.Fatal(fmt.Sprintf("Unexpected num of clients %d", clientCount))
	}

	for c := range s.Clients {
		if c.Name != "UN" {
			t.Fatal(fmt.Sprintf("Client name incorrect, received %s, expected \"UN\"", c.Name))
		}
	}
}

func TestServer_SingleMessage(t *testing.T) {
	s := ServerMake()
	go s.Run(":8100")
	defer s.Close()

	c := ConnectToTestServer("UN", "localhost:8100")
	defer c.Close()

	c.WriteMessage(websocket.TextMessage, []byte("TestMessage"))

	var um UserNamesMsg
	c.ReadJSON(&um)

	if um.Type != "UserNamesMsg" || !reflect.DeepEqual(um.UserNames, []string{"UN"}) {
		t.Fatal(fmt.Sprintf("Un-expected message contents, received %s, expected \"UN: TestMessage\"", um.UserNames))
	}

	var cm ChatEntryMsg
	c.ReadJSON(&cm)

	if cm.Type != "ChatEntryMsg" && cm.Contents != "UN: TestMessage" {
		t.Fatal(fmt.Sprintf("Un-expected message contents, received %s, expected \"UN: TestMessage\"", cm.Contents))
	}
}

// Fires messages from randomised clients to verify that messages are received by the clients in order
func TestServer_Stress(t *testing.T) {
	s := ServerMake()
	go s.Run(":8101")
	defer s.Close()

	const messageCount = 10000
	const workerCount = 10

	type Worker struct {
		workerI    int
		output     []string
		connection *websocket.Conn
	}

	var workers [workerCount]Worker
	var workersDone, workersCreated sync.WaitGroup

	workersDone.Add(workerCount)
	workersCreated.Add(workerCount)

	// Worker pump which connects to the server and receives chat messages
	f := func(w *Worker) {

		w.connection = ConnectToTestServer(strconv.Itoa(w.workerI), "localhost:8101")

		defer func() {
			w.connection.Close()
			fmt.Printf("WorkerFinished %d\n", w.workerI)
			workersDone.Done()
		}()

		var msg ChatEntryMsg
		for {
			w.connection.SetReadDeadline(time.Now().Add(time.Second))
			err := w.connection.ReadJSON(&msg)
			if err != nil {
				if _, ok := err.(*json.InvalidUnmarshalError); !ok {
					break
				}
			}
			if msg.Type == "ChatEntryMsg" {
				w.output = append(w.output, msg.Contents)
				if len(w.output) == messageCount {
					return
				}
			}
		}
	}

	s.ClientConnected = func(clientName string) {
		workersCreated.Done()
	}

	for i := 0; i < workerCount; i++ {
		workers[i].workerI = i
		workers[i].output = make([]string, 0, messageCount)
		go f(&workers[i])
	}

	workersCreated.Wait()

	svrReceivedMsg := make(chan bool)
	s.OnReceived = func(receivedMsg string) {
		svrReceivedMsg <- true
	}

	fmt.Println("All workers created, firing messages")

	expectedOutput := make([]string, 0, messageCount)

	for i := uint32(0); i < uint32(messageCount); i++ {
		workerToFire := rand.Uint32() % uint32(workerCount)
		workers[workerToFire].connection.WriteMessage(websocket.TextMessage, []byte(string(strconv.Itoa(int(i)))))
		<-svrReceivedMsg
		expectedOutput = append(expectedOutput, fmt.Sprintf("%d: %d", workerToFire, i))
	}

	workersDone.Wait()

	for i := range workers {
		if !reflect.DeepEqual(workers[i].output, expectedOutput) {
			t.Fatal("Incorrect output")
		}
	}
}
